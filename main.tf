# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEPLOY TILLER INTO A NEW NAMESPACE
# These templates show an example of how to deploy Tiller following security best practices. This entails:
# - Creating a Namespace and ServiceAccount for Tiller
# - Creating a separate Namespace for the resources to go into
# - Using kubergrunt to deploy Tiller with TLS management
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

terraform {
  required_version = ">= 0.12"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONFIGURE OUR KUBERNETES CONNECTIONS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

provider "kubernetes" {
  config_context = var.kubectl_config_context_name
  config_path    = var.kubectl_config_path
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CREATE THE NAMESPACE WITH RBAC ROLES AND SERVICE ACCOUNT
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

module "tiller_namespace" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-namespace?ref=v0.3.0"
  source = "./modules/k8s-namespace"

  name = var.tiller_namespace
}

module "istio_namespace" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-namespace?ref=v0.3.0"
  source = "./modules/k8s-namespace"

  name = var.istio_namespace
}

module "jenkins_namespace" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-namespace?ref=v0.3.0"
  source = "./modules/k8s-namespace"

  name = var.jenkins_namespace
}

module "resource_namespace" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-namespace?ref=v0.3.0"
  source = "./modules/k8s-namespace"

  name = var.resource_namespace
}

module "tiller_service_account" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-service-account?ref=v0.3.0"
  source = "./modules/k8s-service-account"

  name           = var.service_account_name
  namespace      = module.tiller_namespace.name
  num_rbac_roles = 2

  rbac_roles = [
    {
      name      = module.tiller_namespace.rbac_tiller_metadata_access_role
      namespace = module.tiller_namespace.name
    },
    {
      name      = module.resource_namespace.rbac_tiller_resource_access_role
      namespace = module.resource_namespace.name
    }
  ]

  labels = {
    app = "tiller"
  }
}

module "jenkins_service_account" {
    source = "./modules/k8s-service-account"

    name = var.jenkins_service_account_name
    namespace   = module.jenkins_namespace.name
    num_rbac_roles = 2

    rbac_roles = [
      {
        name      = module.jenkins_namespace.rbac_tiller_metadata_access_role
        namespace = module.jenkins_namespace.name
      },
      {
        name      = module.resource_namespace.rbac_tiller_resource_access_role
        namespace = module.resource_namespace.name
      }
    ]

    labels = {
      app = "jenkins"
    }
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEPLOY TILLER
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

module "tiller" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-tiller?ref=v0.3.0"
  source = "./modules/k8s-tiller"

  tiller_service_account_name              = module.tiller_service_account.name
  tiller_service_account_token_secret_name = module.tiller_service_account.token_secret_name
  namespace                                = module.tiller_namespace.name
  tiller_image_version                     = var.tiller_version

  tiller_tls_gen_method   = "provider"
  tiller_tls_subject      = var.tls_subject
  private_key_algorithm   = var.private_key_algorithm
  private_key_ecdsa_curve = var.private_key_ecdsa_curve
  private_key_rsa_bits    = var.private_key_rsa_bits
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# GENERATE CLIENT TLS CERTIFICATES FOR USE WITH HELM CLIENT
# These certs will be stored in Kubernetes Secrets, in a format compatible with `kubergrunt helm configure`
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

module "helm_client_tls_certs" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "git::https://github.com/gruntwork-io/terraform-kubernetes-helm.git//modules/k8s-helm-client-tls-certs?ref=v0.3.1"
  source = "./modules/k8s-helm-client-tls-certs"

  ca_tls_certificate_key_pair_secret_namespace = module.tiller.tiller_ca_tls_certificate_key_pair_secret_namespace
  ca_tls_certificate_key_pair_secret_name      = module.tiller.tiller_ca_tls_certificate_key_pair_secret_name

  tls_subject                               = var.client_tls_subject
  tls_certificate_key_pair_secret_namespace = module.tiller_namespace.name

  # Kubergrunt expects client cert secrets to be stored under this name format

  tls_certificate_key_pair_secret_name = "tiller-client-${md5(local.rbac_entity_id)}-certs"
  tls_certificate_key_pair_secret_labels = {
    "gruntwork.io/tiller-namespace"        = module.tiller_namespace.name
    "gruntwork.io/tiller-credentials"      = "true"
    "gruntwork.io/tiller-credentials-type" = "client"
  }
}

locals {
  rbac_entity_id = var.grant_helm_client_rbac_user != "" ? var.grant_helm_client_rbac_user : var.grant_helm_client_rbac_group != "" ? var.grant_helm_client_rbac_group : var.grant_helm_client_rbac_service_account != "" ? var.grant_helm_client_rbac_service_account : ""
}

provider "helm" {
  namespace           = var.tiller_namespace
  install_tiller      = false
  enable_tls          = true
  client_key          = module.helm_client_tls_certs.tls_certificate_key_pair_private_key_pem
  client_certificate  = module.helm_client_tls_certs.tls_certificate_key_pair_certificate_pem
  ca_certificate      = module.helm_client_tls_certs.ca_tls_certificate_key_pair_certificate_pem

  kubernetes {
    config_path = "~/.kube/config"
  }
}

data "helm_repository" "istio_repo" {
  name = "istio.io"
  url = "https://storage.googleapis.com/istio-release/releases/1.3.3/charts/"
}

data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

resource "helm_release" "istio_init" {
  name = "istio-init"
  repository = data.helm_repository.istio_repo.metadata.0.name
  chart = "istio-init"
  namespace = var.istio_namespace
  wait = true

  depends_on = ["module.tiller", "module.istio_namespace"]
}

resource "null_resource" "before_istio" {
  provisioner "local-exec" {
    command = "sleep 60"
  }
  triggers = {
    "istio_init" = helm_release.istio_init.id
  }
}

resource "helm_release" "istio" {
  name = "istio"
  repository = data.helm_repository.istio_repo.metadata.0.name
  chart = "istio"
  namespace = var.istio_namespace
  wait = false

  set {
    name = "grafana.enabled"
    value = "true"
  }

  set {
    name = "grafana.security.enabled"
    value = "false"
  }

  set {
    name = "gateways.enabled"
    value = "true"
  }

  depends_on = ["null_resource.before_istio"]
}

provider "k8sraw" {}

resource "k8sraw_yaml" "public_gateway" {
  yaml_body = <<YAML
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: public-gateway
  namespace: istio-system
spec:
  selector:
    istio: ingressgateway
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*.local.exep.live"
  YAML

  depends_on = ["null_resource.before_istio"]
}

resource "k8sraw_yaml" "grafana_virtual_service" {
  yaml_body = <<YAML
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: grafana
  namespace: istio-system
spec:
  hosts:
    - "grafana.local.exep.live"
  gateways:
    - public-gateway
  http:
  - route:
    - destination:
        host: grafana.istio-system.svc.cluster.local
        port:
          number: 3000
    timeout: 30s
  YAML

  depends_on = ["null_resource.before_istio"]
}

resource "helm_release" "jenkins" {
  name = "jenkins"
  namespace = var.jenkins_namespace
  repository = data.helm_repository.stable.metadata.0.name
  chart = "jenkins"

  set {
    name = "master.useSecurity"
    value = "false"
  }

  set {
    name = "master.serviceType"
    value = "ClusterIP"
  }

  set {
    name = "serviceAccount.name"
    value = "jenkins"
  }

  set {
    name = "serviceAccount.create"
    value = "false"
  }

  depends_on = ["module.jenkins_namespace", "module.tiller", "module.jenkins_service_account"]
}

resource "k8sraw_yaml" "jenkins_virtual_service" {
  yaml_body = <<YAML
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: jenkins
  namespace: jenkins
spec:
  hosts:
    - "jenkins.local.exep.live"
  gateways:
    - public-gateway.istio-system.svc.cluster.local
  http:
  - route:
    - destination:
        host: jenkins.jenkins.svc.cluster.local
        port:
          number: 8080
    timeout: 30s
  YAML

  depends_on = ["helm_release.jenkins"]
}

resource "helm_release" "docker_registry" {
  name = "docker-registry"
  namespace = var.jenkins_namespace
  repository = data.helm_repository.stable.metadata.0.name
  chart = "docker-registry"

  depends_on = ["module.jenkins_namespace", "module.tiller"]
}

resource "kubernetes_secret" "minio_secret" {
  metadata {
    name = "minio"
    namespace = var.jenkins_namespace
  }

  data = {
    accesskey = "QUtJQUlPU0ZPRE5ON0VYQU1QTEU="
    secretkey = "d0phbHJYVXRuRkVNSS9LN01ERU5HL2JQeFJmaUNZRVhBTVBMRUtFWQ=="
  }

  depends_on = ["module.jenkins_namespace"]
}

resource "helm_release" "minio" {
  name = "minio"
  namespace = var.jenkins_namespace
  repository = data.helm_repository.stable.metadata.0.name
  chart = "minio"

  set {
    name = "existingSecret"
    value = "minio"
  }

  depends_on = ["kubernetes_secret.minio_secret", "module.tiller"]
}

resource "kubernetes_secret" "chartmuseum_secret" {
  metadata {
    name = "chartmuseum"
    namespace = var.jenkins_namespace
  }

  data = {
    username = "YWRtaW4="
    passphrase = "dW5pY29ybg=="
  }

  depends_on = ["module.jenkins_namespace"]
}

resource "helm_release" "chart_musuem" {
  name = "chartmuseum"
  namespace = var.jenkins_namespace
  repository = data.helm_repository.stable.metadata.0.name
  chart = "chartmuseum"

  set {
    name = "env.open.STORAGE"
    value = "local"
  }

  set {
    name = "env.open.DISABLE_API"
    value = "false"
  }

  set {
    name = "env.persistence.enabled"
    value = "true"
  }

  set {
    name = "env.persistence.accessMode"
    value = "ReadWriteOnce"
  }

  set {
    name = "env.persistence.size"
    value = "4Gi"
  }

  set {
    name = "env.existingSecret"
    value = "chartmuseum"
  }

  set {
    name = "env.existingSecretMappings.BASIC_AUTH_USER"
    value = "username"
  }

  set {
    name = "env.existingSecretMappings.BASIC_AUTH_PASS"
    value = "passphrase"
  }

  depends_on = [
    "helm_release.minio",
    "kubernetes_secret.chartmuseum_secret",
    "module.tiller"
  ]
}